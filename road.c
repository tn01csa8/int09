/*Problem in this code is that the road does not move in a continuous pattern also if i only render 4 lines in a road
those lines render properly but if i exceed the lines more than 4 it does not render properly*/

//For road:
//Properties structure for object line
LineProperties lineObjectConfig[10];
ObjectCfg lineObject[10];
PFbyte lineObjectId[10]={0,0,0,0,0,0,0,0,0,0};
LineProperties lineObjectPrevConfig[10];
DynamicObjectCfg lineObjectDynConfig[10];
void createRoad()
{
	PFword point1x[10]={90,150,90,150,90,150,90,150,90,150};
	PFword point1y[10]={20,20,90,90,150,150,210,210,270,270};
	PFword point2x[10]={90,150,90,150,90,150,90,150,90,150};
	PFword point2y[10]={60,60,130,130,190,190,250,250,310,310};


	PFword i=0;

	for(i=0;i<8;i++)
		{
			lineObjectDynConfig[i].prevObjProperties=&lineObjectPrevConfig[i];
			lineObjectDynConfig[i].speed=4;
			lineObjectDynConfig[i].direction=enSouth;

			lineObjectConfig[i].point1.xValue=point1x[i];
			lineObjectConfig[i].point1.yValue=point1y[i];

			lineObjectConfig[i].point2.xValue=point2x[i];
			lineObjectConfig[i].point2.yValue=point2y[i];

			lineObject[i].name=(PFchar*)"Line object";
			lineObject[i].objShape=enLine;
			lineObject[i].objProperties=&lineObjectConfig[i];
			lineObject[i].color=WHITE;
			lineObject[i].colorFill=enBooleanTrue;
			lineObject[i].visible=enStateVisible;
			lineObject[i].type=enDynamic;
			lineObject[i].dynamicCfg=&lineObjectDynConfig[i];

		createObject(&lineObjectId[i],&lineObject[i]);
	}

	drawAllObjects();
	renderFrame();

}

void moveRoad(int n)
{

	int i=0;
	for(i=0;i<8;i++)
	{


		if(n==0){
			lineObjectConfig[i].point1.yValue+=8;
			lineObjectConfig[i].point2.yValue+=8;


			if(lineObjectConfig[i].point1.yValue==320)
			{
				lineObjectConfig[i].point1.yValue=0;
				lineObjectConfig[i].point2.yValue=40;
			}

		}
		else if(n==1)
		{
			lineObjectConfig[i].point1.yValue+=10;
			lineObjectConfig[i].point2.yValue+=10;

			if(lineObjectConfig[i].point1.yValue==320)
			{
				lineObjectConfig[i].point1.yValue=0;
				lineObjectConfig[i].point2.yValue=40;
			}

		}
	}
	while(lastFrameRendered()==enBooleanFalse)
		for(i=0;i<8;i++){
			updateObject(lineObjectId[i]);
		}
	renderFrame();
}
